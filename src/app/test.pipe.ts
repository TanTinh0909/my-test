import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'test'
})
export class TestPipe implements PipeTransform {

  transform(value: number[], sortnumber: number): any {

    var  result = value.sort((a,b)=>{
      if(a>b) return sortnumber;
      else if(a<b) return -sortnumber;
      else return 0;
    });
    return [...result];
  }

}
