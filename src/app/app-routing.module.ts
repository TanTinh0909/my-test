import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from '../app/home/home.component';
import {LoginComponent} from '../app/login/login.component'
import { CommonModule } from '@angular/common';
import { AuthGuard } from './service/auth.guard';
import { AuthInterceptor } from './service/auth.interceptor';
import { PageNotComponent } from './page-not/page-not.component';
import { ChildrenComponent } from './children/children.component';

import { CanLeaveEditGuard } from './service/can-leave-edit.guard';


const router : Routes = [
    {
        path: 'home', component: HomeComponent,canActivate: [AuthGuard]
    },
    {
        path: 'product/:id', component: ChildrenComponent,
        canActivate: [AuthGuard],
        canDeactivate: [CanLeaveEditGuard]
    },
    {
        path: 'login', component: LoginComponent
    },
    { 
        path: '',  redirectTo: '/home', pathMatch: 'full',canActivate: [AuthGuard]
     },
     { 
        path: '**', component: PageNotComponent
     },
     
] 


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(router)
    ],
    providers: [AuthGuard,AuthInterceptor],
    exports: [RouterModule],
    declarations:[]
  })
  export class AppRoutingModule { }
  