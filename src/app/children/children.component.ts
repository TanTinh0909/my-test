import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ViewChildren, OnChanges, TemplateRef } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { ListUserService } from '../service/list-user.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent implements OnInit {

  @Output() user = new EventEmitter<any>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private listID: ListUserService,
    private router: Router) { }

  listuserId: any[] = [];
  id_ID: number;
  value?: string;

  ngOnInit() {
    this.byIdlistuser();
    // this.user.emit(1234567);
  }

  byIdlistuser() {
    this.activatedRoute.params.subscribe(params => {
      this.id_ID = params['id'];
      if (this.id_ID) {
        this.listID.listUserByid(this.id_ID).subscribe(
          (data) => {
            this.listuserId.push(data.data);
            // console.log(this.listuserId)
          }
        )
      }
    });
  }

  kt_input: boolean = true;
  isEditing: boolean = false;
  isVisible = false;

  edit_List() {
    this.kt_input = false;
    this.isEditing = true;

  }

  goTohome() {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
    this.isEditing = false;
    this.router.navigateByUrl('home');
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  Save_edit_List(value) {
    this.isEditing = false;
    if (this.id_ID = value.id) {
      console.log('value', value);
      this.user.emit(value);
      this.router.navigateByUrl('home');
    }
  }

  checkDeactivate(
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return !this.isEditing;
  }
}
