import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppServiceService } from '../service/app-service.service';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private login: AppServiceService,
    private messenger: NzMessageService
  ) { }

  validateForm!: FormGroup;
  email: any;
  password: any;
  
  // remember: boolean = false;

  // toggleRemember(){
  //   this.remember = ! this.remember;
  //   console.log(this.remember)
  // }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if ((!this.validateForm.invalid)) {
      this.email = this.validateForm.value.email;
      this.password = this.validateForm.value.password;
      this.login.login(this.email, this.password).subscribe(
        (data) => {
          localStorage.setItem('token', data.token);
          localStorage.setItem('email', this.email);
          this.router.navigateByUrl('home');
          this.messenger.success('LOGIN - SUCCESSFUL');
          console.log(data);
          // if(this.remember == true){
          //   console.log(this.remember);
          //   localStorage.setItem('email',this.email);
          //   localStorage.setItem('password',this.password);
          // }
          // this.email = localStorage.getItem('email');
          // this.password = localStorage.getItem('password');
        },
        (error) => {
          console.log(error)
          this.messenger.error(
            'LOGIN - UNSUCCESSFUL',
            {
              nzDuration: 4000
            }
          );
        }
      )
    }
  }
  ngOnInit(): void {

    if (localStorage.getItem('token')) {
      this.router.navigateByUrl('home');
      
    } else {
      localStorage.clear();
      this.router.navigateByUrl('login');
      
    }

    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]]
      // remember: [false]
    });
  }
}
