import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
@Injectable()
@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

    constructor(){}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let tokenCurrent = localStorage.getItem('token');
        if(tokenCurrent){
            req = req.clone({
                setHeaders: 
                {
                    Authorization: tokenCurrent
                }
            })            
        }
        return  next.handle(req);
    }
}
