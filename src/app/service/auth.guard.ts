import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private myRoute: Router) {
  }
  
  canActivate(): boolean {
    
    if (!localStorage.getItem('token')) {
      this.myRoute.navigateByUrl('login');
      return false;
    }
      // this.myRoute.navigateByUrl('home');
      return true;
  }
}
