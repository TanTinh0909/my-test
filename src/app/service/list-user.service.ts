import { Injectable } from '@angular/core';
import {tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListUserService {

  constructor(private http: HttpClient) { }

  ListUser(): Observable<any>{
    return this.http.get<any>('https://reqres.in/api/users')
    .pipe(tap(
      res=>{
        // console.log(res);
      }
    ))
  }
  listUserByid(id): Observable<any>{
    return this.http.get<any>(`https://reqres.in/api/users/${id}`).pipe(tap(
      res => {
        // console.log(res);
      }
    ))
  }
}
