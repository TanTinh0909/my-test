import { Injectable } from '@angular/core';
import {tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any>{
    return this.http.post<any>('https://reqres.in/api/login',{'email':email, 'password': password})
    .pipe(tap(
      (res)=>{
        // console.log(res);
      }
    ))
  }
}
