import { Component, OnInit, HostListener, Input, ViewChild, AfterViewInit } from '@angular/core';
import { ListUserService } from '../service/list-user.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  constructor(private listUser: ListUserService, private activatedRoute: ActivatedRoute) { }


  isCollapsed = false;
  width = 200;
  array = [1, 2, 3, 4];
  isHidden: boolean = false;
  scrWidth: any;
  listuser: any[] = [];
  token: boolean;
  email: any;
  listname: string;
  id_ID: number;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrWidth = window.innerWidth;
    if (this.scrWidth < 860) {
      this.isCollapsed = true;
      this.isHidden = true;
    } else {
      this.isCollapsed = false;
      this.isHidden = false;
    }
  }

  visible: boolean = false;
  hi;
  clickMe(): void {
    this.visible = false;
  }



  ngOnInit() {
    this.getScreenSize();
    this.listUser.ListUser().subscribe(
      (data) => {
        this.listuser = data.data;
      }
    )

    if (localStorage.getItem('token')) {
      this.token = true;
      this.email = localStorage.getItem('email');
    } else {
      this.token = false;
    }
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('email');
  }

  edituser(value: any){
    console.log('result', value);
  }

}
